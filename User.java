
import java.io.FileWriter;
import java.io.IOException;

// json dependencies
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * This is the implementation of the JSON Class user.
 * 
 * @author nchitukula
 *
 */
public class User {

    @SuppressWarnings("unchecked")
	public static void main(String[] args) {

       // Object creation 
    	
		
		JSONObject obj = new JSONObject();
        obj.put("name", "shahsi");
        obj.put("age", 100);
        obj.put("profession", "SoftwareTrainee");
        System.out.println("Original String :" +obj);
        
        // JSON Array object creation
        
        JSONArray hobbies = new JSONArray();
        hobbies.add("Dancing");
        hobbies.add("coding");
        hobbies.add("Eating");

        obj.put("hobby", hobbies);

        try (FileWriter file = new FileWriter("C:\\java programs\\json.txt")) {
            file.write(obj.toJSONString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print("Updated String :" +obj);

    }

    
        
    }

